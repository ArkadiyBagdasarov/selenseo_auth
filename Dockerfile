FROM python:3.6.5
ENV PYTHONUNBUFFERED 1
RUN mkdir /app
RUN mkdir /app/selenseo
COPY . /app/selenseo
WORKDIR /app/selenseo

RUN pip install -r /app/selenseo/deploy_assets/requirements.txt

CMD ["bash", "entrypoint.sh"]
