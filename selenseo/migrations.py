import logging

import peewee

from accounts.models import Account, JWTAccessToken, JWTRefreshToken
from base.models import database

logging.basicConfig(format='%(asctime)s:%(levelname)s:%(message)s',
                    level=logging.INFO)

logger = logging.getLogger()

logger.info('INIT DB')
objects = peewee.PostgresqlDatabase(database)

logger.info('START CREATE TABLES')
Account.create_table(True)
JWTAccessToken.create_table(True)
JWTRefreshToken.create_table(True)
logger.info('END CREATE TABLES')
