import datetime
import hashlib
import jwt

import peewee

from base import settings
from base.models import BaseModel


class Account(BaseModel):
    email = peewee.CharField(max_length=100)
    password = peewee.CharField(max_length=100, null=True)
    balance = peewee.IntegerField(default=0)
    active = peewee.BooleanField(default=True)
    created_at = peewee.DateTimeField(default=lambda: datetime.datetime.now(datetime.timezone.utc))

    class Meta:
        db_table = 'accounts'

    @staticmethod
    async def create(db, **kwargs):
        account = await db.create(
            Account,
            **kwargs
        )
        return account

    @staticmethod
    async def get(db, **kwargs):
        try:
            account = await db.get(
                Account,
                **kwargs
            )
        except Account.DoesNotExist:
            return None

        return account

    @property
    def data(self):
        return {'email': self.email,
                'balance': self.balance}

    async def get_token(self, db):
        access = await db.create(JWTAccessToken, account=self)
        refresh = await db.create(JWTRefreshToken, account=self, access_token=access)

        access_token = access.create_access_token()
        refresh_token = refresh.create_refresh_token()

        return access_token, refresh_token

    async def set_password(self, db, passwd):
        self.password = hashlib.md5(passwd.encode()).hexdigest()
        await db.update(self, ('password',))
        return passwd

    def check_password(self, password_for_check):
        password_hex = hashlib.md5(password_for_check.encode()).hexdigest()

        return self.password == password_hex


class JWTAccessToken(BaseModel):
    account = peewee.ForeignKeyField(Account)
    expires_at = peewee.DateTimeField(default=lambda: (datetime.datetime.now(datetime.timezone.utc)
                                                       + datetime.timedelta(minutes=15)))

    class Meta:
        db_table = 'jwt_access_token'

    def create_access_token(self):
        data = {
            'account': self.account_id,
            'exp': self.expires_at.timestamp(),
            'access_id': self.id

        }
        token = jwt.encode(data, settings.JWT_SECRET, algorithm='HS256')

        return token.decode()


class JWTRefreshToken(BaseModel):
    account = peewee.ForeignKeyField(Account)
    access_token = peewee.ForeignKeyField(JWTAccessToken)
    expires_at = peewee.DateTimeField(default=lambda: (datetime.datetime.now(datetime.timezone.utc)
                                                       + datetime.timedelta(days=60)))

    class Meta:
        db_table = 'jwt_refresh_token'

    def create_refresh_token(self):
        data = {
            'account': self.account_id,
            'exp': self.expires_at.timestamp(),
            'refresh_id': self.id,
            'access_id': self.access_token_id
        }
        token = jwt.encode(data, settings.JWT_SECRET, algorithm='HS256')

        return token.decode()
