import peewee_async

from base.models import database


async def create_db(app):
    database.set_allow_sync(False)
    app['objects'] = peewee_async.Manager(database)


async def close_db(app):
    await app['objects'].close()
