import logging

import jwt
from aiohttp.web import middleware, json_response

from accounts.models import Account, JWTAccessToken
from base import settings


def resp_401():
    resp = json_response({'success': False, 'errors': 'jwt expired'}, status=401)
    resp.set_cookie('ACCESS_TOKEN', '', max_age=0, httponly=False)

    return resp


@middleware
async def get_user(request, handler):
    session = request.cookies.get('ACCESS_TOKEN')
    objects = request.app['objects']
    account = None
    session_user = None

    logging.info(request.cookies)

    if session:
        try:
            token = jwt.decode(session, settings.JWT_SECRET, algorithm='HS256')
        except jwt.ExpiredSignatureError:
            return resp_401()
        except jwt.DecodeError:
            return resp_401()
        try:
            session_user = await objects.get(JWTAccessToken.select(JWTAccessToken, Account)
                                             .join(Account)
                                             .where(JWTAccessToken.id == token['access_id']))
        except JWTAccessToken.DoesNotExist:
            return resp_401()
        else:
            account = session_user.account

    request.account = account
    request.session = session_user

    resp = await handler(request)

    return resp
