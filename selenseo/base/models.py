import peewee
import peewee_async

from base.settings import pg_db_config

database = peewee_async.PooledPostgresqlDatabase(**pg_db_config)


class BaseModel(peewee.Model):
    class Meta:
        database = database
