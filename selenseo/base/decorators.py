import json
from functools import wraps

from aiohttp.web_response import json_response


def get_data(handler):
    @wraps(handler)
    async def wrap(request, *args, **kwargs):
        try:
            data = await request.request.json()
        except json.JSONDecodeError:
            return json_response({'success': False,
                                  'errors': 'json decode error'})
        request.data = data

        return await handler(request, *args, **kwargs)
    return wrap


def check_user(handler):
    @wraps(handler)
    async def wrap(request, *args, **kwargs):
        if not request.request.account:
            return json_response({'success': False, 'errors': 'You not authorized'}, status=403)
        return await handler(request, *args, **kwargs)
    return wrap
