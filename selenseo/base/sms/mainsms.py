import aiohttp  # Для работы с http запросами (в нашем случае POST)
import hashlib  # Для шифрования

from aiohttp import log


class SMS:
    headers = {'Content-type': 'application/x-www-form-urlencoded'}

    def __init__(self, p, a):
        self.project = p  # Имя проекта
        self.api_key = a  # API-ключ
        self.url = 'https://mainsms.ru/api/mainsms/'

    # выполнение запроса
    async def doRequest(self, rqData, url):

        # формируем словарь
        rqData['project'] = self.project  # 1. Добавляем в словарь POST проект
        l = []
        sign = ''

        for i in rqData:  # 2. словарь POST переводим в список
            l.append(str(rqData[i]))
        l.sort()  # 3. сортируем в алфавитном порядке

        for element in l:  # 4. получаем левую часть sign
            sign = sign + str(element) + ';'

        sign = sign + str(self.api_key)  # 5. получаем целый нешифрованный sign

        sign = hashlib.sha1(sign.encode("utf-8")).hexdigest()  # 6. шифруем sha1
        sign = hashlib.md5(sign.encode("utf-8")).hexdigest()  # 7. шифруем md5

        rqData['sign'] = sign  # 8. добавляем sign в POST

        async with aiohttp.ClientSession() as session:
            try:
                async with session.get(self.url + url, headers=self.headers, params=rqData, timeout=5) as resp:
                    log.ws_logger.info('resp status: {}'.format(resp.status))
                    answer = await resp.json()
                    return answer
            except Exception as error:
                log.ws_logger.error('error when send sms; {}'.format(str(error)),
                                    exc_info=True)
                session.close()

    async def sendSMS(self, recipients, message, sender='', run_at='', test=0):  # шлём SMS
        rqData = {
            "recipients": recipients,
            "message": message,
            "test": test
        }
        if sender != '':
            rqData['sender'] = sender

        if run_at != '':
            rqData['run_at'] = run_at
        return await self.doRequest(rqData, 'message/send')

    async def add_in_group(self, phone, sender):
        rqData = {
            "group": 70957,
            "phone": phone,
            "project": 'Phone.verified'
        }

        if sender:
            rqData['sender'] = sender

        return await self.doRequest(rqData, 'contact/create')

