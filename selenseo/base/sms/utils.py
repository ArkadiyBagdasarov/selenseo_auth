import logging

from base.sms import mainsms

logger = logging.getLogger()


async def send_sms(recipients, message):
    project = 'Phone.verified'
    api_key = ' '

    sms = mainsms.SMS(project, api_key)
    resp = await sms.sendSMS(recipients, message, sender=' ')
    logger.info("send sms to: {} message: {}, status: {}".format(recipients, message, resp))


async def add_in_group(phone):
    project = 'Phone.verified'
    api_key = 'f705e0a1c6aeb'

    sms = mainsms.SMS(project, api_key)
    resp = await sms.add_in_group(phone, sender='kingwinch')
    logger.info("add in group: {}, status: {}".format(phone, resp))

