import asyncio

import jwt
from aiohttp import web
from aiohttp.web_response import json_response, Response

from accounts.models import Account, JWTRefreshToken, JWTAccessToken
from api.serializers import auth
from base import settings
from base.decorators import get_data, check_user
from base.middleware import resp_401
from base.models import database


class RegisterView(web.View):
    """
    ---
    description: Метод регистрации.
    tags:
    - auth
    produces:
    - application/json
    parameters:
    - in: body
      name: body
      required: true
      schema:
        type: object
        properties:
          email:
            type: string
            default: "user1234"
          password1:
            type: string
            default: "qwerty"
          password2:
            type: string
            default: "qwerty"
    responses:
        "200":
            description: OK
        "400":
            description: bad request
    """

    serializer_class = auth.RegisterSerializer

    @get_data
    async def post(self):
        result, errors = self.serializer_class().load(self.data)
        if isinstance(errors, Response):
            return errors

        objects = self.request.app['objects']

        account = await Account.get(objects, email=result['email'])

        if account:
            return json_response({'success': False,
                                  'errors': 'this email is already taken'})

        if result['password1'] != result['password2']:
            return json_response({'success': False,
                                  'errors': 'passwords do not match'})

        account = await Account.create(objects, email=result['email'])

        asyncio.ensure_future(account.set_password(objects, result['password1']))

        return json_response({'success': True})


class LoginView(web.View):
    """
    ---
    description: Метод авторизации.
    tags:
    - auth
    produces:
    - application/json
    parameters:
    - in: body
      name: body
      required: true
      schema:
        type: object
        properties:
          email:
            type: string
            default: "test"
          password:
            type: string
            default: "qwerty"

    responses:
        "200":
            description: OK
        "400":
            description: bad request
        "403":
            description: unknown user
    """

    serializer_class = auth.LoginSerializer

    @get_data
    async def post(self):
        result, errors = self.serializer_class().load(self.data)
        if isinstance(errors, Response):
            return errors

        email = result['email']
        password = result['password']

        objects = self.request.app['objects']

        try:
            account = await objects.get(Account, email=email)
        except Account.DoesNotExist:
            return json_response({'success': False,
                                  'errors': 'no such user'},
                                 status=403)

        if not account.check_password(password):
            return json_response({'success': False,
                                  'errors': 'incorrect email or password'},
                                 status=401)

        response = json_response({'success': True})
        access_token, refresh_token = await account.get_token(objects)

        response.set_cookie('ACCESS_TOKEN', access_token, httponly=False)
        response.set_cookie('REFRESH_TOKEN', refresh_token, httponly=False)

        return response


class LogoutView(web.View):
    """
    ---
    description: Метод логаута.
    tags:
    - auth
    produces:
    - application/json

    responses:
        "200":
            description: OK
    """

    @check_user
    async def get(self):
        response = json_response({'success': True})
        response.set_cookie('ACCESS_TOKEN', '', max_age=0, httponly=False)
        response.set_cookie('REFRESH_TOKEN', '', max_age=0, httponly=False)

        objects = self.request.app['objects']

        asyncio.ensure_future(self.remove_tokens(objects, self.request.session))

        return response

    async def remove_tokens(self, objects, access_token):
        async with database.atomic_async():
            await objects.execute(JWTRefreshToken
                                  .delete()
                                  .where(JWTRefreshToken.access_token == access_token))
            await objects.delete(access_token)


class AccountView(web.View):
    """
    ---
    description: Метод просмотра аккаунта.
    tags:
    - auth
    produces:
    - application/json

    responses:
        "200":
            description: OK
    """

    @check_user
    async def get(self):

        return json_response({'success': True,
                              'data': self.request.account.data})


class RefreshTokenView(web.View):
    """
    ---
    description: Метод обновления токенов.
    tags:
    - auth
    produces:
    - application/json

    responses:
        "200":
            description: OK
    """

    async def get(self):
        objects = self.request.app['objects']
        refresh_token = self.request.cookies.get('REFRESH_TOKEN')

        try:
            token = jwt.decode(refresh_token, settings.JWT_SECRET, algorithm='HS256')
        except jwt.ExpiredSignatureError:
            resp = resp_401()
            resp.set_cookie('REFRESH_TOKEN', '', max_age=0, httponly=False)
            return resp
        except jwt.DecodeError:
            resp = resp_401()
            resp.set_cookie('REFRESH_TOKEN', '', max_age=0, httponly=False)
            return resp

        try:
            refresh = await objects.get(JWTRefreshToken.select(JWTRefreshToken, Account, JWTAccessToken)
                                        .join(Account)
                                        .switch(JWTRefreshToken)
                                        .join(JWTAccessToken)
                                        .where(JWTRefreshToken.id == token['refresh_id']))
        except JWTRefreshToken.DoesNotExist:
            resp = resp_401()
            resp.set_cookie('REFRESH_TOKEN', '', max_age=0, httponly=False)
            return resp

        access_token, refresh_token = await refresh.account.get_token(objects)

        response = json_response({
            'success': True,
            'access_token': access_token,
            'refresh_token': refresh_token
        })

        response.set_cookie('ACCESS_TOKEN', access_token, httponly=False)
        response.set_cookie('REFRESH_TOKEN', refresh_token, httponly=False)

        asyncio.ensure_future(self.remove_tokens(objects, refresh))

        return response

    async def remove_tokens(self, objects, refresh):
        async with database.atomic_async():
            await objects.delete(refresh)
            await objects.delete(refresh.access_token)
