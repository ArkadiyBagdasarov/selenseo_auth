from marshmallow import fields

from api.serializers.base import BaseSchema


class RegisterSerializer(BaseSchema):
    email = fields.Email(required=True)
    password1 = fields.Str(required=True)
    password2 = fields.Str(required=True)


class LoginSerializer(BaseSchema):
    email = fields.Email(required=True)
    password = fields.Str(required=True)
