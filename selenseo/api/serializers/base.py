from aiohttp.web_response import json_response
from marshmallow import Schema, ValidationError


class BaseSchema(Schema):
    def load(self, data, many=None, partial=None):
        result, errors = super(BaseSchema, self).load(data, many=many, partial=partial)

        if errors:
            errors, *_ = tuple(errors.values())

            if isinstance(errors, dict):
                errors = tuple(errors.values())

            return None, json_response({'success': False, 'errors': errors[0]})

        return result, None


def min_value(value):
    if value < 1:
        raise ValidationError('can not be less that 1')


def max_validate(max_value):
    def wraps(value):
        if len(value) > max_value:
            raise ValidationError('type not greater than {} symbols'.format(max_value))
    return wraps
